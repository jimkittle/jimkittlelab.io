---
layout: single
title: About
---

I'm an architect working with platform engineering, APIs, data integration/pipelines, and design strategy/roadmapping. 

I enjoy sharing experience to help others and have presented at multiple conferences and meetups.